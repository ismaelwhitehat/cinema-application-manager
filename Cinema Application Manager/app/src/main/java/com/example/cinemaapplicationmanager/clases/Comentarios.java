package com.example.cinemaapplicationmanager.clases;

/**
 * @class Clase Comentarios
 * @author Ismael Burgos
 * @date 2019/12/01
 */

public class Comentarios {

    // DECLARACIÓN DE VARIABLES
    private String comentaPelicula;

    // CONSTRUCTOR
    public Comentarios(String comentaPelicula) {
        this.comentaPelicula = comentaPelicula;
    }
    // GETTERS Y SETTERS
    public String getComentaPelicula() {
        return comentaPelicula;
    }

    public void setComentaPelicula(String comentaPelicula) {
        this.comentaPelicula = comentaPelicula;
    }

    // TO STRING
    @Override
    public String toString() {
        return "Comentarios{" +
                " Comentario:='" + comentaPelicula + '\'' +
                '}';
    }
}
