package com.example.cinemaapplicationmanager;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cinemaapplicationmanager.baseDatos.BaseDeDatos;
import com.example.cinemaapplicationmanager.clases.Pelicula;
import com.example.cinemaapplicationmanager.clases.Registro;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * @class Pantalla Insertar Pelicula a la lista desde SQLITE
 * @author Ismael Burgos
 * @date 2019/12/10
 */

public class PantallaInsertarPelicula extends AppCompatActivity {

    // Objetos de controles

    EditText barraPelicula;
    EditText barraLocalizacionPelicula;
    EditText barraHorarioPelicula;
    EditText barraFechaPelicula;
    EditText barraDuracionPelicula;
    EditText barraGeneroPelicula;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_insertar_pelicula);
        // Asigna el ID del control de elementos
        barraPelicula = (EditText) findViewById(R.id.barraTituloPelicula);
        barraLocalizacionPelicula = (EditText) findViewById(R.id.barraLocalizacion);
        barraHorarioPelicula = (EditText) findViewById(R.id.barraHorario);
        barraFechaPelicula = (EditText) findViewById(R.id.barraFecha);
        barraDuracionPelicula = (EditText) findViewById(R.id.barraDuracion);
        barraGeneroPelicula = (EditText) findViewById(R.id.barraGenero);


    }


    // Añade películas a la base de datos

    public void accionAnadirCartelera(View view) {

        String titulo = barraPelicula.getText().toString();
        String localizacion = barraLocalizacionPelicula.getText().toString();
        String horario = barraHorarioPelicula.getText().toString();
        String Fecha = barraFechaPelicula.getText().toString();
        String duracion = barraDuracionPelicula.getText().toString();
        String genero = barraGeneroPelicula.getText().toString();


        BaseDeDatos bd = new BaseDeDatos(this, "pelicula", 1);
        bd.getWritableDatabase();
        SQLiteDatabase sqLitedatabase = bd.getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put("tituloPelicula", titulo);
        v.put("localizacion", localizacion);
        v.put("horario", horario);
        v.put("fecha", Fecha);
        v.put("duracion", duracion);
        v.put("genero", genero);
        sqLitedatabase.insert("pelicula", null, v);

        Toast.makeText(this, " Título de la película:  " + titulo + " Localización:  " + localizacion + " Horario: " + horario + " Fecha: " + Fecha +
                "Duración: " + duracion + "Género: " + genero, Toast.LENGTH_LONG).show();


      String q = "SELECT * FROM pelicula";
        Cursor registros = sqLitedatabase.rawQuery(q, null);
        if (registros.moveToFirst()) {
            do {
                Pelicula peli = new Pelicula(registros.getString(0),
                            registros.getString(1), registros.getString(2),
                           registros.getString(3), registros.getString(4),
                            registros.getString(5));

                System.out.println(peli.toString());

            } while (registros.moveToNext());
        }

        // Comprueba que no haya campos vacios
        if (titulo.length() == 0) {

            barraPelicula.setError("Debes de introducir el título");
        }

        if (localizacion.length() == 0) {

            barraLocalizacionPelicula.setError("Debes de introducir la localización");
        }

        if (horario.length() == 0) {

            barraHorarioPelicula.setError("Debes de introducir el horario");
        }

        if(Fecha.length()==0){

            barraFechaPelicula.setError("Debe de insertar una fecha");
        }


        if (duracion.length() == 0) {

            barraDuracionPelicula.setError("Debes de introducir la duración");
        }

        if (genero.length() == 0) {

            barraGeneroPelicula.setError("Debes de introducir el género");
        }
    }

    // Vuelve al Menú de opciones

    public void accionVolverMenuInsertar(View view) {

        Intent irMenu=new Intent(this,MenuOpciones.class);
        startActivity(irMenu);
    }
}
