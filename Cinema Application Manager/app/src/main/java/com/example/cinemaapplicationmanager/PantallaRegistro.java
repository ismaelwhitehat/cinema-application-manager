package com.example.cinemaapplicationmanager;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cinemaapplicationmanager.baseDatos.BaseDeDatos;
import com.example.cinemaapplicationmanager.clases.Registro;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * @author Ismael Burgos
 * @class Pantalla de Registro
 * @date 2019/11/16
 */

public class PantallaRegistro extends AppCompatActivity {

    // Objetos de control

    EditText barraRegistro;
    EditText barraContrasena;

    //Declaramos un objeto firebaseAuth
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_registro);
        // Asigna el ID del control de elementos
        barraRegistro = (EditText) findViewById(R.id.BarraCorreoRegistro);
        barraContrasena = (EditText) findViewById(R.id.BarraContrasenaRegistro);
        //inicializamos el objeto firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance();
    }


    // Registra al usuario en la base de datos con su correo electrónico y contraseña
    public void EventoRegistroApp(View view) {

        String correoRegistro=barraRegistro.getText().toString().trim();
        String contrasenaRegistro=barraContrasena.getText().toString().trim();


        //Verificamos que las cajas de texto no esten vacías
        if(TextUtils.isEmpty(correoRegistro)){
            Toast.makeText(this,"Debes de introducir el correo electrónico",Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(contrasenaRegistro)){
            Toast.makeText(this,"Debes de introducir la contraseña",Toast.LENGTH_LONG).show();
            return;
        }

        //Creación de un nuevo usuario
        firebaseAuth.createUserWithEmailAndPassword(correoRegistro, contrasenaRegistro)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //Comprueba si se ha registrado el usuario
                        if(task.isSuccessful()){
                            Toast.makeText(PantallaRegistro.this,"Se ha registrado correctamente ",Toast.LENGTH_LONG).show();
                            Toast.makeText(PantallaRegistro.this, "Bienvenido te has registrado en esta aplicación, gracias por elegir a Cinema Aplication Manager.", Toast.LENGTH_LONG).show();
                            Intent iraprincipal=new Intent(PantallaRegistro.this,PantallaPrincipal.class);
                            startActivity(iraprincipal);
                        }else{

                            Toast.makeText(PantallaRegistro.this,"No se ha podido registrar correctamente ",Toast.LENGTH_LONG).show();
                        }

                    }
                });


    }


}

