package com.example.cinemaapplicationmanager;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cinemaapplicationmanager.baseDatos.BaseDeDatos;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

/**
 * @class Pantalla de login
 * @author Ismael Burgos
 * @date 2019/11/16
 */

public class PantallaLogin extends AppCompatActivity {

    // Objetos de control

    EditText barracorreoLogin;
    EditText barracorreoContrasenaLogin;

    //Declaramos un objeto firebaseAuth
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pantalla_login);

        // Asigna el ID del control de elementos
        barracorreoLogin = (EditText) findViewById(R.id.BarraCorreoLogin);
        barracorreoContrasenaLogin = (EditText) findViewById(R.id.BarraContrasenaLogin);
        //inicializamos el objeto firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance();

    }

    // Se Loguea el usuario si algo va mal, lanza un mensaje al usuario de que no se ha iniciado sesión incorrectamente

    public void accionlogin(View view) {

        String correo = barracorreoLogin.getText().toString().trim();
        String contrasena = barracorreoContrasenaLogin.getText().toString().trim();

        //Verificamos que las cajas de texto no esten vacías
        if(TextUtils.isEmpty(correo)){
            Toast.makeText(this,"Debes de introducir el correo electrónico",Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(contrasena)){
            Toast.makeText(this,"Debes de introducir la contraseña",Toast.LENGTH_LONG).show();
            return;
        }

        //loguear usuario
        firebaseAuth.signInWithEmailAndPassword(correo, contrasena)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // Comprueba si se ha logueado correctamente el usuario
                        if (task.isSuccessful()) {
                            Toast.makeText(PantallaLogin.this, "Se ha iniciado sesión correctamente", Toast.LENGTH_SHORT).show();
                            Intent irpantallaMenu = new Intent(PantallaLogin.this, MenuOpciones.class);
                            startActivity(irpantallaMenu);


                        } else {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {//si se presenta una colisión
                                Toast.makeText(PantallaLogin.this, "Ese usuario ya existe ", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(PantallaLogin.this, "Inicio de sesión incorrecto ", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });

    }
}

