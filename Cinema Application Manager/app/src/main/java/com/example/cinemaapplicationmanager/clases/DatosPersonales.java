package com.example.cinemaapplicationmanager.clases;

/**
 * @class Clase Datos Personales
 * @author Ismael Burgos
 * @date 2019/12/01
 */

public class DatosPersonales {

    // DECLARACIÓN DE VARIABLES
    private String nombre;
    private String apellidos;
    private String correoElectronico;

    // CONSTRUCTOR
    public DatosPersonales(String nombre, String apellidos,String correoElectronico) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.correoElectronico = correoElectronico;
    }

    // GETTERS Y SETTERS
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    // TO STRING
    @Override
    public String toString() {
        return "DatosPersonales{" +
                " Nombre:='" + nombre + '\'' +
                ", Apellidos:='" + apellidos + '\'' +
                ", Correo Electronico:='" + correoElectronico + '\'' +
                '}';
    }
}
