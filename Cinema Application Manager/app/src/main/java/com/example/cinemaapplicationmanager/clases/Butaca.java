package com.example.cinemaapplicationmanager.clases;

/**
 * @class Clase Butaca
 * @author Ismael Burgos
 * @date 2019/12/01
 */

public class Butaca {

    // DECLARACIÓN DE VARIABLES
    private String butacaDisponible;
    private String fila;


    // CONSTRUCTOR
    public Butaca(String butacaDisponible, String fila) {
        this.butacaDisponible = butacaDisponible;
        this.fila = fila;
    }

    // GETTERS Y SETTERS
    public String getButacaDisponible() {
        return butacaDisponible;
    }

    public void setButacaDisponible(String butacaDisponible) {
        this.butacaDisponible = butacaDisponible;
    }

    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    // TO STRING
    @Override
    public String toString() {
        return "Butaca{" +
                " Butaca :=" + butacaDisponible +
                ", Fila :=" + fila +
                '}';
    }
}
