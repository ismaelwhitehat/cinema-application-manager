package com.example.cinemaapplicationmanager.clases;

/**
 * @class Clase Entrada
 * @author Ismael Burgos
 * @date 2019/12/01
 */

public class Entrada {

    // DECLARACIÓN DE VARIABLES

    private String nEntradas;
    private float precio;

    // CONSTRUCTOR
    public Entrada(String nEntradas, float precio) {
        this.nEntradas = nEntradas;
        this.precio = precio;
    }

    // GETTERS Y SETTERS

    public String getnEntradas() {
        return nEntradas;
    }

    public void setnEntradas(String nEntradas) {
        this.nEntradas = nEntradas;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    // TO STRING
    @Override
    public String toString() {
        return "Entrada{" +
                "Número de Entrada:='" + nEntradas + '\'' +
                ", Precio:=" + precio +
                '}';
    }
}
