package com.example.cinemaapplicationmanager.clases;

/**
 * @class Clase Registro
 * @author Ismael Burgos
 * @date 2019/11/23
 */

public class Registro {

    // DECLARACIÓN DE VARIABLES
    private String correoElectronico;
    private String contrasena;

    // CONSTRUCTOR
    public Registro(String correoElectronico, String contrasena) {
        this.correoElectronico = correoElectronico;
        this.contrasena = contrasena;
    }

    // GETTERS Y SETTERS
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }


    // TO STRING
    @Override
    public String toString() {
        return "Registro{" +
                "Correo Electrónico:='" + correoElectronico + '\'' +
                ", Contraseña:='" + contrasena + '\'' +
                '}';
    }
}
