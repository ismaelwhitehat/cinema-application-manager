package com.example.cinemaapplicationmanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

/**
 * @class Pantalla Menú opciones
 * @author Ismael Burgos
 * @date 2019/12/09
 */

public class MenuOpciones extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_opciones);
    }

    // Va hacia la pantalla de Ciudad
    public void pasarComprarEntrada(View view) {

        Intent irCiudad=new Intent(this,PantallaCiudad.class);
        startActivity(irCiudad);
    }

    // Va hacia la pantalla de Comentarios
    public void pasarComentario(View view) {

        Intent ircomentario=new Intent(this,InformacionComentarios.class);
        startActivity(ircomentario);

    }

    // Va hacia la pantalla de Valoraciones
    public void pasarValoraciones(View view) {

        Intent irValoraciones=new Intent(this,InformacionValoraciones.class);
        startActivity(irValoraciones);
    }

    //  Va hacia la pantalla de insertar la película
    public void accionCrearPelicula(View view) {

        Intent irInsertarPelicula=new Intent(this,PantallaInsertarPelicula.class);
        startActivity(irInsertarPelicula);
    }

    // Va hacia la pantalla de Cartelera

    public void accionVerCartelera(View view) {

        Intent irCartelera=new Intent(this,PantallaCartelera.class);
        startActivity(irCartelera);
    }

    // Va hacia la pantalla de borrar la película
    public void accionpantallaBorrar(View view) {

        Intent irpantallaborrar=new Intent(this,PantallaBorrarPelicula.class);
        startActivity(irpantallaborrar);


    }

    // Sale de la aplicación
    public void salir(View view) {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }
}
