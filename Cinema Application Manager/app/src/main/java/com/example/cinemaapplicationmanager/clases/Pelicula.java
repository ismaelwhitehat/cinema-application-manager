package com.example.cinemaapplicationmanager.clases;

/**
 * @class Clase Película
 * @author Ismael Burgos
 * @date 2019/12/01
 */

public class Pelicula {

    // DECLARACIÓN DE VARIABLES
    private String tituloPelicula;
    private String localizacion;
    private String horario;
    private String fecha;
    private String duracion;
    private String genero;


    // CONSTRUCTOR
    public Pelicula(String tituloPelicula, String localizacion, String horario, String fecha, String duracion, String genero) {
        this.tituloPelicula = tituloPelicula;
        this.localizacion = localizacion;
        this.horario = horario;
        this.fecha = fecha;
        this.duracion = duracion;
        this.genero = genero;
    }

    // GETTERS Y SETTERS

    public String getTituloPelicula() {
        return tituloPelicula;
    }

    public void setTituloPelicula(String tituloPelicula) {
        this.tituloPelicula = tituloPelicula;
    }

    public String getLocalizacion() {
        return localizacion;
    }

    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    // TO STRING
    @Override
    public String toString() {
        return "Pelicula{" +
                " Título de la película:='" + tituloPelicula + '\'' +
                ", Localización:='" + localizacion + '\'' +
                ", Horario:=" + horario +
                ", Fecha:=" + fecha +
                ", Duración:=" + duracion +
                ", Género:='" + genero + '\'' +
                '}';
    }
}
