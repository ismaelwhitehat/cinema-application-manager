package com.example.cinemaapplicationmanager;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cinemaapplicationmanager.baseDatos.BaseDeDatos;
import com.example.cinemaapplicationmanager.clases.Comentarios;
import com.example.cinemaapplicationmanager.clases.DatosPersonales;
import com.example.cinemaapplicationmanager.clases.Registro;

/**
 * @class Pantalla de los datos personales
 * @author Ismael Burgos
 * @date 2019/11/16
 */

public class InformacionDatosPersonales extends AppCompatActivity {

    // Objetos de control
    EditText Nombre;
    EditText Apellidos;
    EditText CorreoElectronico;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_personales);

        // Asigna el ID de los controles de elementos
        Nombre=(EditText)findViewById(R.id.barraNombreDatosPersonales);
        Apellidos=(EditText)findViewById(R.id.barraApellidosDatosPersonales);
        CorreoElectronico=(EditText)findViewById(R.id.barraCorreoElectronicoDatosPersonales);


    }

    // Inserta en la base de datos los datos personales
    public void accionSiguienteDatosPersonales(View view) {

        String NombreDatosPersonales=Nombre.getText().toString();
        String ApellidosDatosPersonales=Apellidos.getText().toString();
        String CorreoElectronicoDatosPersonales=CorreoElectronico.getText().toString();

        BaseDeDatos bd = new BaseDeDatos(this, "datosPersonales", 1);
        bd.getWritableDatabase();
        SQLiteDatabase sqLitedatabase = bd.getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put("nombre", NombreDatosPersonales);
        v.put("apellidos", ApellidosDatosPersonales);
        v.put("correoElectronico", CorreoElectronicoDatosPersonales);

        sqLitedatabase.insert("datosPersonales", null, v);

        String q = "SELECT * FROM datosPersonales";
        Cursor registros = sqLitedatabase.rawQuery(q,null);


        if(registros.moveToFirst()){
            do{
                DatosPersonales datos=new DatosPersonales(registros.getString(0),registros.getString(1),registros.getString(2));
                System.out.println(datos.toString());

            }while(registros.moveToNext());
        }

        Toast.makeText(this," Nombre : "+ NombreDatosPersonales + " Apellidos : "+ ApellidosDatosPersonales + " Correo Electrónico :" + CorreoElectronicoDatosPersonales ,Toast.LENGTH_LONG).show();

        // Comprueba que no haya campos vacios

        if(NombreDatosPersonales.length()==0){

            Nombre.setError("Debes de introducir el nombre");
        }

        if(ApellidosDatosPersonales.length()==0){

            Apellidos.setError("Debes de introducir el apellido");
        }

        if(CorreoElectronicoDatosPersonales.length()==0){

            CorreoElectronico.setError("Debes de introducir el correo Electrónico");
        }

        // Si ha ido bien pasa a la pantalla de butaca
        if(NombreDatosPersonales.length()!=0 && ApellidosDatosPersonales.length()!=0 && CorreoElectronicoDatosPersonales.length()!=0){
            Intent irButaca=new Intent(this, InformacionButaca.class);
            startActivity(irButaca);
        }

    }
}
