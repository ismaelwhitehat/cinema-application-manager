package com.example.cinemaapplicationmanager;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.cinemaapplicationmanager.baseDatos.BaseDeDatos;
import com.example.cinemaapplicationmanager.clases.Butaca;
import com.example.cinemaapplicationmanager.clases.DatosPersonales;
import com.example.cinemaapplicationmanager.clases.Sala;

import java.util.ArrayList;
import java.util.List;

/**
 * @class Pantalla Sala
 * @author Ismael Burgos
 * @date 2019/11/30
 */

public class InformacionSala extends AppCompatActivity {

    // Objetos controles

    EditText barraSalaInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sala);

        // Asigna el ID al control de elementos
        barraSalaInfo=(EditText)findViewById(R.id.barraSala);


    }

    // Inserta la sala en la base de datos

    public void accionInsertarSala(View view) {

        String saladisp = barraSalaInfo.getText().toString();


        BaseDeDatos bd = new BaseDeDatos(this, "sala", 1);
        bd.getWritableDatabase();
        SQLiteDatabase sqLitedatabase = bd.getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put("salaDisponible", saladisp);
        sqLitedatabase.insert("sala", null, v);

        String q = "SELECT * FROM sala";
        Cursor registros = sqLitedatabase.rawQuery(q, null);
        if (registros.moveToFirst()) {
            do {
                Sala sa=new Sala(registros.getString(0));
                System.out.println(sa.toString());

            } while (registros.moveToNext());
        }

        Toast.makeText(this, "Sala:" + saladisp , Toast.LENGTH_SHORT).show();

        // Comprueba de que no haya campo vacio
        if (saladisp.length() == 0) {
            barraSalaInfo.setError("Debes de introducir una sala");
        }


    }

    // Comprueba si la sala insertada  existe en la base de datos, en el caso contrario, se envia un mensaje
    // notificando al usuario de que no existe

    public void accionComprobarSiguiente(View view) {

        String ConsultarSala = barraSalaInfo.getText().toString();

        BaseDeDatos bd = new BaseDeDatos(this, "sala", 1);
        bd.getReadableDatabase();
        SQLiteDatabase sqLitedatabase = bd.getReadableDatabase();

        String[] campos = new String[]{"salaDisponible"};
        String[] valores = new String[]{ConsultarSala};


        // Comprueba de que no haya campo vacio

        if (ConsultarSala.length() == 0) {

            barraSalaInfo.setError("Debes de introducir una sala");


        } else {

            Cursor registros = sqLitedatabase.query("sala", campos, "salaDisponible = ? ", valores, null, null, null);

            if (registros != null) {
                if (registros.moveToFirst()) {
                    // Si ha ido bien pasa a la pantalla de Entrada
                    Intent irEntrada=new Intent(this, InformacionEntrada.class);
                    startActivity(irEntrada);

                } else {
                    Toast.makeText(this, "La sala no existe", Toast.LENGTH_LONG).show();
                }
            }


        }


    }

    }

