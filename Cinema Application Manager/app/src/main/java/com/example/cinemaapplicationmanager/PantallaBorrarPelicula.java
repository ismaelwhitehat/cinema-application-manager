package com.example.cinemaapplicationmanager;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cinemaapplicationmanager.baseDatos.BaseDeDatos;

/**
 * @class Pantalla para borrar la película
 * @author Ismael Burgos
 * @date 2019/12/11
 */

public class PantallaBorrarPelicula extends AppCompatActivity {

    // Objetos de controles
    EditText barraborrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_borrar_pelicula);

        // Asigna el ID del control de elementos
        barraborrar=(EditText)findViewById(R.id.barraBorrarPelicula);
    }

    // Borra la película insertada en la base de datos
    public void accionBorrarPelicula(View view) {

        String borrarDato=barraborrar.getText().toString();

        BaseDeDatos bdborrar=new BaseDeDatos(this,"pelicula",1);
        bdborrar.getWritableDatabase();
        SQLiteDatabase database = bdborrar.getWritableDatabase();
        int contador =database.delete("pelicula", "tituloPelicula='" + borrarDato + "'", null);
        if (contador !=0){
            Toast.makeText(this,"Eliminado correctamente:" + borrarDato , Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this,"No existe la película:" + borrarDato , Toast.LENGTH_LONG).show();
        }
        database.close();

        // Comprueba de que no haya campo vacio
        if(borrarDato.length()==0){

           barraborrar.setError("Debes de introducir el nombre de la película para eliminarla");

        }
    }


    // Vuelve al Menú de opciones

    public void accionVolverMenuBorrar(View view) {

        Intent irMenu=new Intent(this,MenuOpciones.class);
        startActivity(irMenu);
    }
}
