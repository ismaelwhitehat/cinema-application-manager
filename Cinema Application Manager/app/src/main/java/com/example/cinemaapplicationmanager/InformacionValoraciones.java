package com.example.cinemaapplicationmanager;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cinemaapplicationmanager.baseDatos.BaseDeDatos;
import com.example.cinemaapplicationmanager.clases.Entrada;
import com.example.cinemaapplicationmanager.clases.Registro;
import com.example.cinemaapplicationmanager.clases.Valoraciones;

/**
 * @class Pantalla para que los usuarios puedan valorar las películas
 * @author Ismael Burgos
 * @date 2019/11/16
 */

public class InformacionValoraciones extends AppCompatActivity {

    // Objetos de controles

    RatingBar rb;
    TextView textoResultadoRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valoraciones);

        // Asigna el ID al control de elementos
        rb=(RatingBar)findViewById(R.id.estrellasValoracion);
        textoResultadoRating=(TextView) findViewById(R.id.txtResultadoValoracion);

        // Escuchador de evento por el cual sirve para hacer el funcionamiento del elemento en cuestión
        rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                String valorRating=String.valueOf(rating);

                textoResultadoRating.setText(valorRating);
            }
        });
    }
    // Inserta la valoración en la base de datos

    public void accionSiguienteValoraciones(View view) {

        String textoValoracionInsertar=textoResultadoRating.getText().toString();


        BaseDeDatos bd = new BaseDeDatos(this, "valoraciones", 1);
        bd.getWritableDatabase();
        SQLiteDatabase sqLitedatabase = bd.getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put("valoraPelicula", textoValoracionInsertar);
        sqLitedatabase.insert("valoraciones", null, v);


        String q = "SELECT * FROM valoraciones";
        Cursor registros = sqLitedatabase.rawQuery(q,null);
        if(registros.moveToFirst()){
            do{
                Valoraciones va=new Valoraciones(registros.getString(0));
                System.out.println(va.toString());

            }while(registros.moveToNext());
        }

        Toast.makeText(this, " Valoración :"+ textoValoracionInsertar ,Toast.LENGTH_SHORT).show();

        // Comprueba que no haya campo vacio
        if(textoValoracionInsertar.length()==0){

            Toast.makeText(this,"Debes de valorar la película",Toast.LENGTH_LONG).show();
        }

        // Si ha ido bien Vuelve al Menú de opciones
        if(textoValoracionInsertar.length()!=0){

            Intent irValoracionMenu=new Intent(this,MenuOpciones.class);
            startActivity(irValoracionMenu);

        }

    }
}

