package com.example.cinemaapplicationmanager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.cinemaapplicationmanager.baseDatos.BaseDeDatos;
import com.example.cinemaapplicationmanager.clases.Pelicula;

import java.util.ArrayList;

/**
 * @class Pantalla Cartelera
 * @author Ismael Burgos
 * @date 2019/11/17
 */

public class PantallaCartelera extends AppCompatActivity {

    // Objetos de control
    ListView listaCart;
    ArrayList<Pelicula>listaPeli;
    ArrayAdapter adaptadorPelicula;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_cartelera);

        // Asigna el ID del control de elementos
        listaCart=(ListView)findViewById(R.id.listaPeliculas);
        // Instancia de la base de datos para conseguir el método de llenar Cartelera, unirlo con un adaptador
        // y así conseguir que la cartelera se vaya llenando con los datos insertados
        BaseDeDatos bd=new BaseDeDatos(getApplicationContext(),"pelicula",1);
        listaPeli = bd.llenarCartelera();
        adaptadorPelicula = new ArrayAdapter(this, android.R.layout.simple_list_item_1,listaPeli);
        listaCart.setAdapter(adaptadorPelicula);

    }
    
}