package com.example.cinemaapplicationmanager;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cinemaapplicationmanager.baseDatos.BaseDeDatos;
import com.example.cinemaapplicationmanager.clases.Ciudad;


/**
 * @class Pantalla de la provincia del cine con sus respectictivas ciudades
 * @author Ismael Burgos
 * @date 2019/11/16
 */

public class PantallaCiudad extends AppCompatActivity {

    // Objetos de control
    EditText editCiudad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_ciudad);
        // Asigna el ID del control de elementos
        editCiudad=(EditText)findViewById(R.id.editbarraCiudad);

    }


    // Inserta la Ciudad en la base de datos
    public void accionCiudad(View view) {

        String ciudadInsertar=editCiudad.getText().toString();
        BaseDeDatos bd = new BaseDeDatos(this, "ciudad", 1);
        bd.getWritableDatabase();
        SQLiteDatabase sqLitedatabase = bd.getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put("nombreCiudad", ciudadInsertar);
        sqLitedatabase.insert("ciudad", null, v);


        String q = "SELECT * FROM ciudad";
        Cursor registros = sqLitedatabase.rawQuery(q,null);
        if(registros.moveToFirst()){
            do{
                Ciudad ci=new Ciudad(registros.getString(0));
                System.out.println(ci.toString());
            }while(registros.moveToNext());
        }

        Toast.makeText(this, " Ciudad :" + ciudadInsertar ,Toast.LENGTH_SHORT).show();

        // Comprueba que no haya campos vacios
        if(ciudadInsertar.length()==0){

            editCiudad.setError("Debes de introducir la ciudad");
        }

        if(ciudadInsertar.length()!=0){
            Intent irDatosPersonales=new Intent(this, InformacionDatosPersonales.class);
            startActivity(irDatosPersonales);
        }

    }
}

