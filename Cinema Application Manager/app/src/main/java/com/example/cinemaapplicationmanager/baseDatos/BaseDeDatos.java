package com.example.cinemaapplicationmanager.baseDatos;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.example.cinemaapplicationmanager.clases.Pelicula;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

/**
 * @class Clase que tendrá la base de datos del cine entero
 * @author Ismael Burgos
 * @date 2019/11/23
 */

public class BaseDeDatos extends SQLiteOpenHelper {

    // Constructor que contiene el contexto, el nombre y la versión de la base de datos

    public BaseDeDatos(Context context, String name, int version) {

        super(context, name, null, version);

    }
    // Método que permite la creación de las tablas de la base de datos
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table ciudad(nombreCiudad varchar);");
        db.execSQL("create table pelicula(tituloPelicula varchar, localizacion varchar, horario varchar, fecha varchar, duracion varchar, genero varchar);");
        db.execSQL("create table butaca(butacaDisponible varchar,fila varchar);");
        db.execSQL("create table sala(salaDisponible varchar);");
        db.execSQL("create table datosPersonales(nombre varchar,apellidos varchar,correoElectronico varchar);");
        db.execSQL("create table comentarios(comentaPelicula varchar);");
        db.execSQL("create table valoraciones(valoraPelicula varchar);");
        db.execSQL("create table entrada(nentrada varchar, precio float);");
    }

    // Método que permite actualizar el objeto SQLiteDatabase, la versión y la nueva versión

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    // Método que devuelve una lista para que el usuario pueda insertar una película y poder verla en la cartelera

    public ArrayList llenarCartelera() {
        ArrayList<Pelicula> lista = new ArrayList<>();
        SQLiteDatabase database = this.getWritableDatabase();
        String q = "SELECT * FROM pelicula";
        Cursor registros = database.rawQuery(q, null);
        if (registros.moveToFirst()) {
            do {
                Pelicula peliculaLista = new Pelicula(registros.getString(0),
                            registros.getString(1), registros.getString(2),
                            registros.getString(3), registros.getString(4),
                            registros.getString(5));
                lista.add(peliculaLista);


            } while (registros.moveToNext());
        }

       return lista;
    }

    }
