package com.example.cinemaapplicationmanager.clases;

/**
 * @class Clase Sala
 * @author Ismael Burgos
 * @date 2019/12/01
 */

public class Sala {

    // DECLARACIÓN DE VARIABLES
    private String salaDisponible;

    // CONSTRUCTOR
    public Sala(String salaDisponible) {
        this.salaDisponible = salaDisponible;
    }

    // GETTERS Y SETTERS
    public String getSalaDisponible() {
        return salaDisponible;
    }

    public void setSalaDisponible(String salaDisponible) {
        this.salaDisponible = salaDisponible;
    }

    // TO STRING
    @Override
    public String toString() {
        return "Sala{" +
                " Número de sala:=" + salaDisponible +
                '}';
    }
}
