package com.example.cinemaapplicationmanager;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cinemaapplicationmanager.baseDatos.BaseDeDatos;
import com.example.cinemaapplicationmanager.clases.Butaca;
import com.example.cinemaapplicationmanager.clases.Ciudad;
import com.example.cinemaapplicationmanager.clases.Comentarios;
import com.example.cinemaapplicationmanager.clases.DatosPersonales;
import com.example.cinemaapplicationmanager.clases.Entrada;
import com.example.cinemaapplicationmanager.clases.Pelicula;
import com.example.cinemaapplicationmanager.clases.Sala;
import com.example.cinemaapplicationmanager.clases.Valoraciones;

import java.util.ArrayList;
import java.util.List;

/**
 * @class Clase Entrada
 * @author Ismael Burgos
 * @date 2019/11/30
 */

public class InformacionEntrada extends AppCompatActivity {

    // variable contador
    private int contador=0;

    // Objetos de controles
     TextView barraPrecio;
    Button asignarMenos;
    Button asignarMas;
    Button ComprarEntrada;
    TextView textoaumentodisminuir;
    TextView precioValor;
    TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrada);

        // Asigna los ID de los controles de elementos

        barraPrecio=(TextView)findViewById(R.id.txtprecioAumentoDisminuir);
        asignarMenos=(Button)findViewById(R.id.btnMenos);
        asignarMas=(Button)findViewById(R.id.btnMas);
        ComprarEntrada=(Button)findViewById(R.id.btnComprarEntrada);
        textoaumentodisminuir=(TextView)findViewById(R.id.txtprecioAumentoDisminuir);
        precioValor=(TextView)findViewById(R.id.txtPrecioValor);
        resultado=(TextView)findViewById(R.id.txtResultado);

    }

    // Botón que decrementa el número de entradas

    public void accionbotonMenos(View view) {

        if(contador>0){
            contador=contador-1;
            String contadorMenosTexto=String.valueOf(contador);
            barraPrecio.setText(contadorMenosTexto);
            float valor4=Float.parseFloat(precioValor.getText().toString());
            float valor3=Float.parseFloat(textoaumentodisminuir.getText().toString());
            float re=valor4-valor3*15/100;
            String conversormenos=String.valueOf(re);
            resultado.setText(conversormenos);
        }


    }

    // Botón que incrementa a uno el número de entradas

    public void accionbotonMas(View view) {

        if (contador < 1) {
            contador = contador + 1;
            String contadorMasTexto = String.valueOf(contador);
            barraPrecio.setText(contadorMasTexto);
            float valor1 = Float.parseFloat(textoaumentodisminuir.getText().toString());
            float valor2 = Float.parseFloat(precioValor.getText().toString());
            float re = valor2 + valor1 * 15 / 100;
            String conversorMas = String.valueOf(re);
            resultado.setText(conversorMas);
        }

    }

    // Inserta en la base de datos el número de entradas y el precio para poder comprarlo

    public void accionComprarEntrada(View view) {

        String entradaTexto=textoaumentodisminuir.getText().toString();
        String resultadoCompra=resultado.getText().toString();

        BaseDeDatos bd = new BaseDeDatos(this, "entrada", 1);
        bd.getWritableDatabase();
        SQLiteDatabase sqLitedatabase = bd.getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put("nentrada", entradaTexto);
        v.put("precio",resultadoCompra);
        sqLitedatabase.insert("entrada", null, v);


        String q = "SELECT * FROM entrada";
        Cursor registros = sqLitedatabase.rawQuery(q,null);
        if(registros.moveToFirst()){
            do{
                Entrada e=new Entrada(registros.getString(0),registros.getFloat(1));
                System.out.println(e.toString());

            }while(registros.moveToNext());
        }

        Toast.makeText(this, " Número de entradas :" + entradaTexto + "Precio: "+ resultadoCompra  ,Toast.LENGTH_LONG).show();

        // Comprueba que no haya campo vacio
        if(textoaumentodisminuir.length()==0){

            Toast.makeText(this,"Debes de seleccionar cuantas entradas quieres, para poder comprar",Toast.LENGTH_LONG).show();
        }

    }

    // Vuelve al Menú de las opciones

    public void volverMenuEntrada(View view) {

        Intent irMenuOpciones=new Intent(this,MenuOpciones.class);
        startActivity(irMenuOpciones);
    }
}
















