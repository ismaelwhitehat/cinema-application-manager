package com.example.cinemaapplicationmanager;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.cinemaapplicationmanager.baseDatos.BaseDeDatos;
import com.example.cinemaapplicationmanager.clases.Butaca;
import com.example.cinemaapplicationmanager.clases.Registro;
import com.example.cinemaapplicationmanager.clases.Sala;

import java.util.ArrayList;
import java.util.List;

/**
 * @class Pantalla Butaca
 * @author Ismael Burgos
 * @date 2019/11/30
 */

public class InformacionButaca extends AppCompatActivity {

    // Objetos de controles
    EditText butaca;
    EditText fila;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_butaca);

        // Asigna ID a los controles de los elementos
        butaca = (EditText) findViewById(R.id.barraButacaDisponible);
        fila = (EditText) findViewById(R.id.barraFilaButaca);

    }

    // Inserta en la base de datos la butaca y la fila

    public void accionInsertarButaca(View view) {

        String butacaDisp = butaca.getText().toString();
        String filadisp = fila.getText().toString();

        BaseDeDatos bd = new BaseDeDatos(this, "butaca", 1);
        bd.getWritableDatabase();
        SQLiteDatabase sqLitedatabase = bd.getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put("butacaDisponible", butacaDisp);
        v.put("fila", filadisp);
        sqLitedatabase.insert("butaca", null, v);


        String q = "SELECT * FROM butaca";
        Cursor registros = sqLitedatabase.rawQuery(q, null);
        if (registros.moveToFirst()) {
            do {
                Butaca bu = new Butaca(registros.getString(0), registros.getString(1));
                System.out.println(bu.toString());

            } while (registros.moveToNext());
        }

        Toast.makeText(this, "Butaca:" + butacaDisp + "Fila: " + filadisp, Toast.LENGTH_SHORT).show();


        // Comprueba si la butaca y la fila no tiene campos vacios

        if (butacaDisp.length() == 0) {
            butaca.setError("Debes de introducir una butaca");

        }

        if (filadisp.length() == 0) {
            fila.setError("Debes de introducir la fila");
        }


    }

    // Comprueba si la butaca y la fila está en la base de datos, en caso contrario, notifica al usuario de que
    // la butaca y la fila no existe

    public void accionComprobarSiguiente(View view) {
        String ConsultarButaca = butaca.getText().toString();
        String ConsultarFila = fila.getText().toString();


        BaseDeDatos bd = new BaseDeDatos(this, "butaca", 1);
        bd.getReadableDatabase();
        SQLiteDatabase sqLitedatabase = bd.getReadableDatabase();

        String[] campos = new String[]{"butacaDisponible", "fila"};
        String[] valores = new String[]{ConsultarButaca, ConsultarFila};

        // Comprueba de que la butaca y fila no tenga campos vacios
        if (ConsultarButaca.length() == 0 || ConsultarFila.length() == 0) {

            butaca.setError("Debes de introducir una butaca");
            fila.setError("Debes de introducir una fila");

        } else {

            Cursor registros = sqLitedatabase.query("butaca", campos, "butacaDisponible = ? AND fila = ?", valores, null, null, null);

            if (registros != null) {
                if (registros.moveToFirst()) {

                    // Si ha ido bien pasa a la siguiente pantalla
                    Intent irsala = new Intent(this, InformacionSala.class);
                    startActivity(irsala);

                } else {
                    Toast.makeText(this, "Butaca o fila no existe", Toast.LENGTH_LONG).show();
                }
            }


        }


    }
}

