package com.example.cinemaapplicationmanager;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cinemaapplicationmanager.baseDatos.BaseDeDatos;
import com.example.cinemaapplicationmanager.clases.Comentarios;
import com.example.cinemaapplicationmanager.clases.Registro;
import com.example.cinemaapplicationmanager.clases.Valoraciones;

/**
 * @class Pantalla para comentar los usuarios
 * @author Ismael Burgos
 * @date 2019/11/16
 */

public class InformacionComentarios extends AppCompatActivity {

    // Objetos de controles

    EditText comentario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentarios);

        // Asigna ID al control del elemento
        comentario=(EditText)findViewById(R.id.barracomentaPelicula);
    }

   // Inserta el comentario en la base de datos

    public void accionSiguienteComentarios(View view) {

        String comentaPelicula=comentario.getText().toString();

        BaseDeDatos bd = new BaseDeDatos(this, "comentarios", 1);
        bd.getWritableDatabase();
        SQLiteDatabase sqLitedatabase = bd.getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put("comentaPelicula", comentaPelicula);
        sqLitedatabase.insert("comentarios", null, v);


        String q = "SELECT * FROM comentarios";
        Cursor registros = sqLitedatabase.rawQuery(q,null);
        if(registros.moveToFirst()){
            do{
                Comentarios co=new Comentarios(registros.getString(0));
                System.out.println(co.toString());

            }while(registros.moveToNext());
        }

        Toast.makeText(this, " Comentario :"+ comentaPelicula ,Toast.LENGTH_LONG).show();


         // Comprueba de que no haya campo vacio
        if(comentaPelicula.length()==0){

            comentario.setError("Debes de comentar la película");
        }

        if(comentaPelicula.length()!=0){
            Intent irMenuOpciones=new Intent(this,MenuOpciones.class);
            startActivity(irMenuOpciones);
        }

    }
}
