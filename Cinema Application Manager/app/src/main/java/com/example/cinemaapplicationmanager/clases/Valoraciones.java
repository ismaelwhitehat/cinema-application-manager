package com.example.cinemaapplicationmanager.clases;

/**
 * @class Clase Valoraciones
 * @author Ismael Burgos
 * @date 2019/12/01
 */

public class Valoraciones {

    // DECLARACIÓN DE VARIABLES
    private String valoraPelicula;

    // CONSTRUCTOR
    public Valoraciones(String valoraPelicula) {
        this.valoraPelicula = valoraPelicula;
    }

    // GETTERS Y SETTERS
    public String getValoraPelicula() {
        return valoraPelicula;
    }

    public void setValoraPelicula(String valoraPelicula) {
        this.valoraPelicula = valoraPelicula;
    }

    // TO STRING
    @Override
    public String toString() {
        return "Valoraciones{" +
                " Valoración:='" + valoraPelicula + '\'' +
                '}';
    }
}
