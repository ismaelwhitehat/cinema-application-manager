package com.example.cinemaapplicationmanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

/**
 * @class Pantalla Principal del Cine
 * @author Ismael Burgos
 * @date 2019/11/16
 */

public class PantallaPrincipal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pantalla_principal);
    }

    // Va hacia la pantalla para registrarse

    public void accionRegistro(View view) {

        Intent registro=new Intent(this,PantallaRegistro.class);
        startActivity(registro);
    }

    // Va hacia la pantalla para loguearse

    public void accionLogin(View view) {

        Intent login=new Intent(this,PantallaLogin.class);
        startActivity(login);


    }
}
