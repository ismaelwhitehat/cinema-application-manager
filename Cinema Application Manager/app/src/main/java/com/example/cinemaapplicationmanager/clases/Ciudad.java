package com.example.cinemaapplicationmanager.clases;

/**
 * @class Clase Ciudad
 * @author Ismael Burgos
 * @date 2019/12/01
 */

public class Ciudad {

    // DECLARACIÓN DE VARIABLES
    private String nombreCiudad;

    // CONSTRUCTOR
    public Ciudad(String nombreCiudad) {
        this.nombreCiudad = nombreCiudad;
    }

    // GETTERS Y SETTERS
    public String getNombreCiudad() {
        return nombreCiudad;
    }
    public void setNombreCiudad(String nombreCiudad) {
        this.nombreCiudad = nombreCiudad;
    }

    // TO STRING
    @Override
    public String toString() {
        return "Ciudad{" +
                " Nombre de la ciudad:='" + nombreCiudad + '\'' +
                '}';
    }
}

